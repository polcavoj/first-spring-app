### First simple Spring App

Jednoduché REST API nad tabulkou produktů dle struktury níže. Vytvořeno s použitím Spring Initializr a H2 Database.

<b>tbl_products</b><br>
| ID | TYPE  | NAME | QUANTITY |

Pro zapnutí aplikace slouží příkaz v domovské složce projektu (s pom.xml).
```
mvn spring-boot:run
```

Databázi lze procházet pomocí zabudované konzole ve webovém prohlížeči - <i>localhost:8080/console</i>

![Konzolový přístup](console.png "Console H2")

Pomocí příkazové řádky a následujících příkazů lze otestovat funkčnost.

GET
- List všech produktů - <i>curl localhost:8080/api/products/</i>
- Konkrétní produkt - <i>curl localhost:8080/api/products/{id}</i>

POST
- Přidání produktu - <i>curl -d "{Request body}" -H "Content-Type: application/json" -X POST localhost:8080/api/products/</i>

```
curl -d "{\"type\":\"zelenina\", \"name\":\"meluon\", \"quantity\":\"52\"}" -H "Content-Type: application/json" -X POST localhost:8080/api/products
```

PUT
- Úprava produktu - <i>curl -d "{Request body}" -H "Content-Type: application/json" -X POST localhost:8080/api/products/{id}</i>

```
curl -d "{\"type\":\"zelenina\", \"name\":\"meluon\", \"quantity\":\"52\"}" -H "Content-Type: application/json" -X PUT localhost:8080/api/products/2
```

DELETE
- Smazání produktu - <i> curl -X DELETE localhost:8080/api/products/{id}</i>
