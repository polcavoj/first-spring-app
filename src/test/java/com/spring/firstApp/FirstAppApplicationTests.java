package com.spring.firstApp;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.*;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.HttpClientErrorException;
//import org.junit.jupiter.api.Test;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = FirstAppApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class FirstAppApplicationTests {

	@Autowired
	private TestRestTemplate restTemplate;

	@LocalServerPort
	private int port;

	private String getRootUrl() {
		return "http://localhost:" + port;
	}

	@Test
	void contextLoads() {
	}


	@Test
	public void testGetAllUsers() {
		HttpHeaders headers = new HttpHeaders();
		HttpEntity<String> entity = new HttpEntity<String>(null, headers);

		ResponseEntity<String> response = restTemplate.exchange(getRootUrl() + "/products",
				HttpMethod.GET, entity, String.class);

		Assert.assertNotNull(response.getBody());
	}


	@Test
	public void testGetUserById() {
		Products product = restTemplate.getForObject(getRootUrl() + "/products/1", Products.class);
		Assert.assertNotNull(product);
	}

	@Test
	public void testCreateUser() {
		Products product = new Products();
		product.setType("testovaci");
		product.setName("test");
		product.setQuantity(100);

		ResponseEntity<Products> postResponse = restTemplate.postForEntity(getRootUrl() + "/products", product, Products.class);
		Assert.assertNotNull(postResponse);
		Assert.assertNotNull(postResponse.getBody());
	}

	@Test
	public void testUpdatePost() {
		int id = 1;
		Products product = restTemplate.getForObject(getRootUrl() + "/products/" + id, Products.class);
		product.setType("admin1");
		product.setName("admin2");

		restTemplate.put(getRootUrl() + "/products/" + id, product);

		Products updatedUser = restTemplate.getForObject(getRootUrl() + "/products/" + id, Products.class);
		Assert.assertNotNull(updatedUser);
	}

	@Test
	public void testDeletePost() {
		int id = 2;
		Products product = restTemplate.getForObject(getRootUrl() + "/products/" + id, Products.class);
		Assert.assertNotNull(product);

		restTemplate.delete(getRootUrl() + "/products/" + id);

		try {
			product = restTemplate.getForObject(getRootUrl() + "/products/" + id, Products.class);
		} catch (final HttpClientErrorException e) {
			Assert.assertEquals(e.getStatusCode(), HttpStatus.NOT_FOUND);
		}
	}

}
