package com.spring.firstApp;

import com.spring.firstApp.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface ProductsRepository extends JpaRepository<Products, Long>{
}
