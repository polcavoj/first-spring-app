package com.spring.firstApp.REST;

import com.spring.firstApp.Products;
import com.spring.firstApp.ProductsRepository;
import com.spring.firstApp.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class Controller {

    @Autowired
    private ProductsRepository productsRepository;

    /**
     * Get all products list.
     *
     * @return the list
     */
    @GetMapping("/products")
    public List<Products> getAllProducts() {
        return productsRepository.findAll();
    }
    /**
     * Gets products by id.
     *
     * @param productId the product id
     * @return the products by id
     */
    @GetMapping("/products/{id}")
    public ResponseEntity<Products> getProductsById(@PathVariable(value = "id") Long productId)
        throws ResourceNotFoundException {
            Products product = productsRepository
                    .findById(productId)
                            .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));
        return ResponseEntity.ok().body(product);
    }
    /**
     * Create Product.
     *
     * @param Product
     * @return the Product
     */
    @PostMapping("/products")
    public Products createProducts(@RequestBody Products product) {
        return productsRepository.save(product);
    }
    /**
     * Update product response entity.
     *
     * @param productId the product id
     * @param productDetails the product details
     * @return the response entity
     * @throws ResourceNotFoundException the resource not found exception
     */
    @PutMapping("/products/{id}")
    public ResponseEntity<Products> updateProduct(
            @PathVariable(value = "id") Long productId, @RequestBody Products productDetails)
        throws ResourceNotFoundException {
            Products product =
                productsRepository
                        .findById(productId)
                        .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));

            product.setType(productDetails.getType());
            product.setName(productDetails.getName());
            product.setQuantity(productDetails.getQuantity());
            final Products updatedProduct = productsRepository.save(product);
            return ResponseEntity.ok(updatedProduct);
    }
    /**
     * Delete product map.
     *
     * @param productId the product id
     * @return the map
     * @throws Exception the exception
     */
    @DeleteMapping("/products/{id}")
    public Map<String, Boolean> deleteProduct(@PathVariable(value = "id") Long productId) throws Exception {
        Products product =
                productsRepository
                        .findById(productId)
                        .orElseThrow(() -> new ResourceNotFoundException("Product not found on :: " + productId));
        productsRepository.delete(product);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}